import Papa from "papaparse";
import db from "./db";
import tableRender from "./tableRender";

const peopleImporter = async (e) => {
  let file = e.target?.files;
  let dbInst = await db;
  if (file.length) {
    Papa.parse(file[0], {
      header: true,
      step: async (row, parser) => {
        let person = row.data;
        if (!person.email.length) {
          return;
        }
        const item = {
          name: person.name,
          email: person.email,
          division: person.division,
          department: person.department,
          reports: person.reports,
          staff: person.staff,
        };
        await dbInst.add("people", item).catch((e) => {});
        renderPeople();
      },
    });
  }
};

const renderPeople = (targetId = "peopleTable") => {
  let deleteButtonRender = (row) => {
    return (
      "<button class='removePerson btn btn-primary' data-email='" +
      row.email +
      "'>Remove</button>"
    );
  };
  db.then((db) => {
    db.getAll("people").then((people) => {
      tableRender(
        targetId,
        ["name", "email", "division", deleteButtonRender],
        people
      );
      let deleteButtons = document.getElementsByClassName("removePerson");
      for (let button of deleteButtons) {
        button.addEventListener("click", (e) => {
          removePerson(e.target.dataset.email, e.target.parentElement.parentElement);
        });
      }
      downloadPeople(people);
    });
  });
};

const downloadPeople = async (people) => {
  let csv = "data:text/csv;charset=utf-8," + Papa.unparse(people);
  let target = document.getElementById("downloadPeople");
  target.href = encodeURI(csv);
}

const removePerson = async (email, target) => {
  let dbInit = await db;
  dbInit.delete("people", email);
  target.remove();
};

export { peopleImporter, renderPeople, removePerson };
