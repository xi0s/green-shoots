import { openDB } from "idb";

const db = openDB("green-shoots", 5, {
  upgrade(dbConfig) {
    console.log("Creating a new object store.");
    if (!dbConfig.objectStoreNames.contains("people")) {
      const peopleDb = dbConfig.createObjectStore("people", {
        keyPath: "email",
      });
      peopleDb.createIndex("email", "email", { unique: true });
      peopleDb.createIndex("name", "name", { unique: false });
    }
    if (!dbConfig.objectStoreNames.contains("matches")) {
      const matchesDb = dbConfig.createObjectStore("matches", {
        keyPath: "id",
        autoIncrement: true,
      });
      matchesDb.createIndex("email1", "email1", { unique: false });
      matchesDb.createIndex("email2", "email2", { unique: false });
    }
  },
});

export default db;
