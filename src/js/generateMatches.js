import db from "./db";
import { renderMatches } from "./matches";
import tableRender from "./tableRender";
import Papa from "papaparse";

let maxRun = 100;

let matches = [];

const generateMatches = async (e) => {
  let matches = await calculateMatches();

  document.getElementById("matchesOutput").style.display = "block";

  renderNewMatches(matches.matches, matches.unmatched);
};

const renderNewMatches = (matches, unmatched) => {
  tableRender("generatedMatches", ["name1", "name2"], matches);
  tableRender("unmatched", ["name", "email"], unmatched);
};

const calculateMatches = async () => {
  let dbInst = await db;
  let people = await dbInst.getAll("people");
  let existingMatches = await dbInst.getAll("matches");

  console.log("Generating");

  let unmatched = [];

  let i = 0;
  startMatch: while (people.length > 1) {
    i++;
    if (i > maxRun) {
      break;
    }
    let person1 = people.splice(
      Math.floor(Math.random() * people.length),
      1
    )[0];
    console.debug("Person 1", person1);
    let person2;
    let person2match = false;
    let d = 1;
    identifyPerson2: while (!person2match) {
      d++;
      if (d > 5) {
        unmatched.push(person1);
        continue startMatch;
      }
      let pos = Math.floor(Math.random() * people.length);

      person2 = people.slice(pos, pos + 1)[0];
      console.debug("Person 2", person2);
      if (
        person1.division == person2.division &&
        person1.division != undefined
      ) {
        console.debug("Ending due to same division");
        continue;
      }
      if (
        person1.department == person2.department &&
        person1.department != undefined
      ) {
        console.debug("Ending due to same department");
        continue;
      }

      if (existingMatches.length) {
        let existingMatch = existingMatches.find((element) => {
          if (
            person1.email == element.email1 &&
            person2.email == element.email2
          ) {
            return true;
          }
          if (
            person1.email == element.email2 &&
            person2.email == element.email1
          ) {
            return true;
          }
          return false;
        });
        console.debug("Existing Match: ", existingMatch);
        if (existingMatch) {
          console.debug("Ending due to existing match");
          continue;
        }
      }

      people.splice(pos, 1);
      person2match = true;
      console.debug(people.length + " people remaining");
    }

    matches.push({
      email1: person1.email,
      name1: person1.name,
      email2: person2.email,
      name2: person2.name,
    });
  }

  unmatched = unmatched.concat(people);

  console.log(matches);
  let csv = "data:text/csv;charset=utf-8," + Papa.unparse(matches);
  let saveMatchesElem = document.getElementById("saveMatches");
  saveMatchesElem.href = encodeURI(csv);

  return {
    matches: matches,
    unmatched: unmatched,
  };
};

const saveMatches = async () => {
  let dbInit = await db;
  matches.forEach(async (match) => {
    await dbInit.add("matches", match);
  });
  renderMatches();
  
  matches = [];
  document.getElementById("matchesOutput").style.display = "none";
};

export { generateMatches, saveMatches };
