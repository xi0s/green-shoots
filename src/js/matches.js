import Papa from "papaparse";
import db from "./db";
import tableRender from "./tableRender";

const matchesImporter = async (e) => {
  let file = e.target?.files;
  let dbInst = await db;
  if (file.length) {
    Papa.parse(file[0], {
      header: true,
      step: async (row) => {
        let match = row.data;
        console.debug(match);
        const item = {
          name1: match.name1,
          email1: match.email1,
          name2: match.name2,
          email2: match.email2,
        };
        return await dbInst.add("matches", item).catch((e) => {});
      },
      complete: () => {
        renderMatches();
      },
    });
  }
};

const renderMatches = (targetId = "matchesTable") => {
  db.then((db) => {
    db.getAll("matches").then((matches) => {
      tableRender(targetId, ["name1", "name2"], matches);
      downloadMatches(matches);
    });
  });
};

const downloadMatches = async (matches) => {
  let csv = "data:text/csv;charset=utf-8," + Papa.unparse(matches);
  let target = document.getElementById("downloadMatches");
  target.href = encodeURI(csv);
}

export { matchesImporter, renderMatches };
