const tableRender = (tableIdRef, cols, data) => {
  const table = document.getElementById(tableIdRef);
  const tbody = table.querySelector("tbody");
  tbody.innerHTML = "";
  data.forEach((datum) => {
    let row = tbody.insertRow();
    cols.forEach((col) => {
      let cell = row.insertCell();
      if (typeof col === "function") {
        cell.innerHTML = col.call(undefined, datum);
      } else {
        cell.innerHTML = datum[col];
      }
    });
  });
};

export default tableRender;
