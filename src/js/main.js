// Import our custom CSS
import "../scss/styles.scss";

// Import local modules
import { renderPeople, peopleImporter } from "./people";
import { renderMatches, matchesImporter } from "./matches";
import { generateMatches, saveMatches } from "./generateMatches";

renderPeople();
renderMatches();

const peopleFileElem = document.getElementById("peopleFile");
peopleFileElem.addEventListener("change", peopleImporter);

const matchesFileElem = document.getElementById("matchesFile");
matchesFileElem.addEventListener("change", matchesImporter);

const generateMatchesElem = document.getElementById("generateMatches");
generateMatchesElem.addEventListener("click", generateMatches);

const saveMatchesElem = document.getElementById("saveMatches");
saveMatchesElem.addEventListener("click", saveMatches);